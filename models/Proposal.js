const mongoose = require("mongoose");

// schema used to structure data sent to mongoDB, uses mongoose to do so
const ProposalSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  proposal_title: {
    type: String,
    required: true
  },
  proposal_category: {
    type: String
  },
  proposal_details: {
    type: String
  },
  company: {
    type: String
  },
  industry: {
    type: String
  },
  phone_number: {
    type: String
  },
  website: {
    type: String
  }
});

module.exports = Proposal = mongoose.model("proposal", ProposalSchema);
