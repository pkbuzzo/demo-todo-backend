const express = require("express");
const connectDB = require("./config/db");
var cors = require("cors");

// routes
const proposal = require("./routes/api/proposals");

const app = express();

// Connect Database
connectDB();

// cors
app.use(cors({ origin: true, credentials: true }));

// Init Middleware
app.use(express.json({ extended: false }));

app.get("/", (req, res) => res.send("Our Kick-Ass Mars API Is Working!"));

// use Routes
app.use("/api/proposals", proposal);

const port = process.env.PORT || 8082;

app.listen(port, () => console.log(`Server running on port ${port}`));

// Reference: https://blog.logrocket.com/mern-stack-a-to-z-part-1/
