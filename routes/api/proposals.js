const express = require("express");
const router = express.Router();

// Load Proposal model
const Proposal = require("../../models/Proposal");

// GET api/proposals/test
// description tests proposals route
router.get("/test", (req, res) => res.send("proposal route testing!"));

// GET api/proposals
// description Get all proposals
router.get("/", (req, res) => {
  Proposal.find()
    .then(proposals => res.json(proposals))
    .catch(err =>
      res.status(404).json({ noproposalsfound: "No Proposals found" })
    );
});

// GET api/proposals/:id
// description Get single proposal by id
router.get("/:id", (req, res) => {
  Proposal.findById(req.params.id)
    .then(proposal => res.json(proposal))
    .catch(err =>
      res.status(404).json({ noproposalfound: "No Proposal found" })
    );
});

// GET api/proposals
// description add/save proposal
router.post("/", (req, res) => {
  Proposal.create(req.body)
    .then(proposal => res.json({ msg: "Proposal added successfully" }))
    .catch(err =>
      res.status(400).json({ error: "Unable to add this proposal" })
    );
});

// GET api/proposals/:id
// description Update proposal
router.put("/:id", (req, res) => {
  Proposal.findByIdAndUpdate(req.params.id, req.body)
    .then(proposal => res.json({ msg: "Updated successfully" }))
    .catch(err =>
      res.status(400).json({ error: "Unable to update the Database" })
    );
});

// GET api/proposals/:id
// description Delete proposal by id
router.delete("/:id", (req, res) => {
  Proposal.findByIdAndRemove(req.params.id, req.body)
    .then(proposal => res.json({ mgs: "Proposal entry deleted successfully" }))
    .catch(err => res.status(404).json({ error: "No such a proposal" }));
});

module.exports = router;
